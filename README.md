˹|GoSt PROJECT|˺
---:|:---:|:---
-| *ALL YOU NEED TO KNOW ABOUT SETTING UP THE GAME!* |-
-| [![Logo#0](http://p337.info/main/icons/csgo-icon.png)](http://store.steampowered.com/app/730/?l=czech) |-

˹|˺
---:|:---
[![CZE](http://statni.vlajky.org/32/ceska_republika.png)](https://bitbucket.org/AlennGK/gost/wiki/Home)| [![ENG](http://www.postexpo.com/images/flags/uk.png)](https://bitbucket.org/AlennGK/gost/wiki/Home_EN)

---
# PROJECT TREE: → GO ↑ AND PICK LANGUAGE
* **0.0 PROGRESS PLAN**
    * Tell me if you see something misspelled.
    * Fix links in Home_EN
    * SER.cfg | 20%
    * SET.cfg | 70%

* [1.0 CONFIG](https://bitbucket.org/AlennGK/gost/wiki/Home_EN#markdown-header-10-config) ![Logo#1](https://cdn4.iconfinder.com/data/icons/10-line-icons-set-for-ui-more/32/edit-01-24.png)
	* 1.1 Sound: ![Logo#2](https://cdn4.iconfinder.com/data/icons/10-line-icons-set-for-ui-more/32/sos-01-24.png)
	* 1.2 Models ![Logo#3](https://cdn3.iconfinder.com/data/icons/social-media-chat-1/512/Cubenet-24.png)
	* 1.3 Cross ![Logo#4](https://cdn4.iconfinder.com/data/icons/catholic-line/259/4-32.png)
	* 1.4 Font ![Logo#5](https://cdn3.iconfinder.com/data/icons/searchicons/338/Searchicons-search-text-24.png)
	* 1.5 Commands ![Logo#7](https://cdn2.iconfinder.com/data/icons/snipicons/500/application-code-m-32.png)
	* 1.6 GUI ![Logo#7](https://cdn3.iconfinder.com/data/icons/linecons-free-vector-icons-pack/32/eye-32.png)

* [2.0 WINDOWS](https://bitbucket.org/AlennGK/gost/wiki/Home_EN#markdown-header-20-windows)  ![Logo#10](https://cdn4.iconfinder.com/data/icons/brands/512/microsoft-32.png)

	* 2.1 Mouse ![Logo#11](https://cdn2.iconfinder.com/data/icons/freecns-cumulus/16/519641-142_Mouse-24.png)
	* 2.2 Graphic ![Logo#12](https://cdn2.iconfinder.com/data/icons/bitsies/128/Brush-24.png)
	* 2.3 Internet ![Logo#12](https://cdn3.iconfinder.com/data/icons/linecons-free-vector-icons-pack/32/world-24.png)

* [3.0 STEAM & GAME](https://bitbucket.org/AlennGK/gost/wiki/Home_EN#markdown-header-30-steam-game) ![Logo#20](https://cdn3.iconfinder.com/data/icons/sympletts-free-sampler/128/controller-console-24.png)

* [4.0 GAME TIPS & OTHER](https://bitbucket.org/AlennGK/gost/wiki/Home_EN#markdown-header-40-game-tips-other) ![Logo#30](https://cdn1.iconfinder.com/data/icons/social-media-set/30/Metacafe-32.png)

* [5.0 ABOUT](https://bitbucket.org/AlennGK/gost/wiki/Home_EN#markdown-header-50-about) ![Logo#40](https://cdn2.iconfinder.com/data/icons/free-basic-icon-set-2/300/19-24.png)
	* 5.1 Included Contents ![Logo#50](https://cdn1.iconfinder.com/data/icons/material-core/18/unfold-more-24.png)

* [6.0 INSTALLATION](https://bitbucket.org/AlennGK/gost/wiki/Home_EN#markdown-header-60-installation) ![Logo#55](https://cdn2.iconfinder.com/data/icons/interface-12/24/interface-62-24.png)

* **![REVIEW](./Preview.png)**
-   You can thank me on [![AlennGK](https://cdn3.iconfinder.com/data/icons/linecons-free-vector-icons-pack/32/mail-32.png)](mailto:AlennGK@gmail.com) or ↓. **Or better support me** [![Donate#1](https://cdn2.iconfinder.com/data/icons/food-drink-10/24/food-drink-29-32.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LD9A9XDASUAAE) or in my comments.┊ [![Link#2](https://cdn0.iconfinder.com/data/icons/layout-and-location/24/Untitled-2-36-32.png)](https://steamcommunity.com/app/730/discussions/0/412448792351680217/?tscn=1457011624) ┊ [![Twitter](https://cdn2.iconfinder.com/data/icons/social-media-2046/32/twitter_social_media_online-32.png)](https://twitter.com/Alenn_GK)┊

##### Before this: [counter.mirovicko.net/nastaveni-CS1.6](http://goo.gl/PMCzOJ)


---
### #Thank you:

˹|ˉ|ˉ|˺
:---|:---:|---:|---:
***#Alenn***![Logo#55](http://www.adiumxtras.com/images/thumbs/vaultboy_20_fallout_2_10193_5526_thumb.gif) | [![CZE](http://statni.vlajky.org/32/ceska_republika.png)](https://en.wikipedia.org/wiki/Czech_Republic) | [![License: CC BY-NC-ND ***2/2016***  4.0](https://img.shields.io/badge/license-CC%20BY--NC--ND%204.0-blue.svg)](http://creativecommons.org/licenses/by-nc-nd/4.0/) __ **v1.9a** __ | **Icons provided through [IconFinder](https://www.iconfinder.com): All credits to creators (in given links), licence same or less (free!).**


┊||┊
:---|---:|---:
[![Steam](http://badges.steamprofile.com/profile/default/steam/76561198020965797.png)](https://steamcommunity.com/id/AlennGK) | **I'VE JUST SAVED YOU `2-20 or more hours`... consider donute .-)** | [![Donate#1](https://cdn2.iconfinder.com/data/icons/food-drink-10/24/food-drink-29-128.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LD9A9XDASUAAE)

˹|ˉ|ˉ|˺
:----|:---:|:---:|---:
[![Valve:](http://blog.counter-strike.net/wp-content/themes/counterstrike_launch/images/footer_valve.png)](http://www.valvesoftware.com/games/csgo.html) | ***© 2013 Valve Corporation*** | | [![Legal info:](http://www.valvesoftware.com/images/title_legal.png)](http://www.valvesoftware.com/legal.html)
˻|ˍ|ˍ|˼